const mongoose = require('mongoose')
const {
    Schema
} = mongoose
const userSchema = new Schema({
    fName: String,
    lName: String,
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    userStatus: {
        type: String,
        default: "active"
    },
    resetpass: {
        type: String,
        default: ""
    }

})
module.exports = mongoose.model('user', userSchema)