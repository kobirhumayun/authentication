const Joi = require('joi')
const regiSchemavalidation = Joi.object({
    fName: Joi.string(),
    lName: Joi.string(),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    confirmPassword: Joi.string()




})
const resetPassValidator = Joi.object({
    email: Joi.string().email()
})
module.exports = {
    regiSchemavalidation,
    resetPassValidator
}