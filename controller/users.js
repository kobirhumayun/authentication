const registrationRes = (req, res) => {
    res.status(200).send(req.body)
}
const loginRes = (req, res) => {
    res.status(200).send({
        mgs: 'login successfully',
        token: res.body.token
    })
}
const resetPassRes = (req, res) => {
    res.status(200).send({
        mgs: 'reset password successfully'
    })
}

module.exports = {
    registrationRes,
    loginRes,
    resetPassRes
}