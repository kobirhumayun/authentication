const express = require('express')
const app = express()
const mongoose = require('mongoose')
require('dotenv').config()
const port = process.env.port
const users = require('./route/users')
app.use(express.json())
app.use('/user', users)


app.all('*', (req, res) => {
    res.status(400).send('API not found')
})
app.listen(port, () => {
    console.log(`server running on port ${port}`)
})
mongoose.connect('mongodb://localhost:27017/resetpass')