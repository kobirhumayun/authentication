const {
    regiSchemavalidation,
    resetPassValidator
} = require('../validator/users')
const user = require('../model/users')
const res = require('express/lib/response')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require("nodemailer");


const registration = async (req, res, next) => {

    try {
        const {
            err
        } = await regiSchemavalidation.validateAsync(req.body)
        if (err) {
            res.status(401).send({
                msg: "validation error",
                err: error.details[0].message
            })
        } else {
            if (req.body.password === req.body.confirmPassword) {
                delete req.confirmPassword
                const hash = await bcrypt.hash(req.body.password, 10)
                req.body.password = hash
                const userData = new user(req.body)
                const result = await userData.save()
                next()
            } else {
                res.status(400).send('password not match')
            }


        }
    } catch (error) {
        res.status(400).send(error)
    }
}
const login = async (req, res, next) => {
    try {
        const findData = await user.findOne({
            email: req.body.email
        }, {
            _id: 0,
            password: 1,
            email: 1
        })
        const bool = await bcrypt.compare(req.body.password, findData.password)
        if (bool) {
            const resData = JSON.parse(JSON.stringify(findData))
            delete resData.password
            const token = jwt.sign(resData, process.env.privateKey, {
                expiresIn: '10h'
            })
            res.body = {}
            res.body.token = token
            next()

        } else {
            res.status(400).send('wrong password')
        }
    } catch (error) {
        res.status(400).send(error)
    }
}

const codeGen = () => {
    return Math.floor(Math.random() * 6000) + 1000
}
const resetPass = async (req, res, next) => {
    try {
        const {
            err
        } = await resetPassValidator.validateAsync(req.body)
        if (err) {
            res.status(401).send({
                msg: "validation error",
                err: error.details[0].message
            })
        } else {
            const transporter = nodemailer.createTransport({
                service: "gmail",
                auth: {
                    user: 'kobirhumayunhr@gmail.com',
                    pass: process.env.mailpass
                },
            });
            const varificationCode = codeGen()
            let mailOption = {
                from: 'kobirhumayunhr@gmail.com',
                to: req.body.email,
                subject: "Hello",
                html: `<h2>verification code</h2></br><h1>${varificationCode}</h1>`
            }

            transporter.sendMail(mailOption, (err, data) => {
                if (err) {
                    res.status(400).send('verification code sending failed')
                } else {
                    const token = jwt.sign({
                        email: req.body.email
                    }, process.env.privateKey, {
                        expiresIn: '10h'
                    })
                    res.status(200).send({
                        msg: 'verification code sent to your mail',
                        token
                    })
                }
            })

            await user.updateOne({
                email: req.body.email
            }, {
                $set: {
                    resetpass: varificationCode
                }
            })

        }
    } catch (error) {
        res.status(400).send(error)
    }


}


const resetPassCode = async (req, res, next) => {
    try {
        const token = req.body.token
        const decoded = jwt.verify(token, process.env.privateKey);

        const findData = await user.findOne({
            email: decoded.email
        })
        const codeData = JSON.parse(JSON.stringify(findData))
        if (codeData.resetpass === req.body.code) {
            if (req.body.password === req.body.confirmPassword) {
                delete req.confirmPassword
                const hash = await bcrypt.hash(req.body.password, 10)
                await user.updateOne({
                    email: decoded.email
                }, {
                    $set: {
                        password: hash,
                        resetpass: ""
                    }
                })

                next()
            } else {
                res.status(400).send('password not match')
            }
        } else {
            res.status(400).send('invalid code')
        }

    } catch (error) {
        res.status(401).send(error)
    }

}



module.exports = {
    registration,
    login,
    resetPass,
    resetPassCode
}