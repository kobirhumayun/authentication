const express = require('express')
const route = express.Router()
const {
    registration,
    login,
    resetPass,
    resetPassCode
} = require('../middleware/users')
const {
    registrationRes,
    loginRes,
    resetPassRes
} = require('../controller/users')
route.post('/login', login, loginRes)
route.post('/registration', registration, registrationRes)
route.post('/resetpass', resetPass)
route.post('/resetpass/code', resetPassCode, resetPassRes)
route.get('/data')

module.exports = route